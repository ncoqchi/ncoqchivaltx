import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {RequerimientoService} from './services/requerimiento.service';
import {ReactiveFormsModule} from '@angular/forms';

import { SucursalComponent } from './components/sucursal/sucursal.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import {ProductoComponent} from './components/producto/producto.component';
import {ProductoService} from './services/producto.service';
import {UsuarioService} from './services/usuario.service';
import {SucursalService} from './services/sucursal.service';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductoComponent,
    SucursalComponent,
    UsuarioComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [RequerimientoService, ProductoService, UsuarioService, SucursalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
