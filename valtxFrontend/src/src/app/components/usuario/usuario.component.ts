import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Usuario} from "../../models/usuario";
import {UsuarioService} from "../../services/usuario.service";
import {HttpErrorResponse} from "@angular/common/http";
import {SucursalService} from "../../services/sucursal.service";
import {Sucursal} from "../../models/sucursal";

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuarioForm: FormGroup = new FormGroup({});
  public usuarios : Usuario[] = [];
  public sucursales : Sucursal[] = [];
  flagShowForm : boolean=false;
  showButtonRegistrar : boolean=false;
  showButtonActualizar : boolean=false;

  constructor(private formBuilder: FormBuilder,private usuarioService: UsuarioService, private sucursalService:SucursalService) { }

  ngOnInit(): void {

    this.listar();


    //get sucursales
    this.sucursalService.listar().subscribe(
      (response : Sucursal[]) => {

        console.log(response);
        this.sucursales = response;
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );


    this.usuarioForm = this.formBuilder.group({
      codUsuario : [''],
      nombre : [''],
      user : [''],
      password : [''],
      codSucursal :['xx'],
    });


  }


  public showForm(){
    this.flagShowForm = true;

    console.log(this.flagShowForm);
  }

  public cancelarForm(){

    this.flagShowForm = false;
    console.log(this.flagShowForm)
  }
  public nuevo() : void {

    this.flagShowForm = true;
    this.showButtonRegistrar = true;
    this.showButtonActualizar = false;

    this.usuarioForm.patchValue({
      codUsuario : '',
      nombre: '',
      user : '',
      password : '',
      codSucursal : 'xx'
    });
  }



  public editar(usuario: Usuario): void{

    this.flagShowForm = true;
    this.showButtonRegistrar = false;
    this.showButtonActualizar = true;

    console.log(usuario);
    this.usuarioForm.patchValue({
      codUsuario : usuario.codUsuario,
      nombre: usuario.nombre,
      user : usuario.user,
      password : usuario.password,
      codSucursal : usuario.codSucursal
    });


  }

  public eliminar( codUsuario: string) : void {

    var a = confirm("¿ Desea eliminar el usuario Seleccionado ?");
    console.log(a);
    if(a){
      this.usuarioService.eliminar(codUsuario).subscribe(
        (response : number) => {
          console.log(response);
          alert("Usuario Eliminado");
          this.listar();
        },
        (error: HttpErrorResponse) => {
          console.error(error);
        }
      );
    }

  }

  public registrar():void {

    if(this.usuarioForm.value.codSucursal == 'xx'){
      alert('Debe seleccionar una sucursal');
    }else if(this.usuarioForm.value.codUsuario == ''){
      alert('Debe ingresar un código de usuario de 2 caracteres');
    }else if(this.usuarioForm.value.nombre == ''){
      alert('Debe ingresar un nombre');
    }else if(this.usuarioForm.value.user == ''){
      alert('Debe ingresar un nombre de usuario');
    }else if(this.usuarioForm.value.password == ''){
      alert('Debe ingresar una password');
    }
    else{
      this.usuarioService.registrar(this.usuarioForm.value).subscribe(
        (response : number) => {
          console.log(response);
          alert("Usuario Registrado");

          this.showButtonRegistrar = true;
          this.showButtonActualizar = false;
          this.flagShowForm = false;
          this.listar();

        },
        (error: HttpErrorResponse) => {
          console.error(error);
          if(error.status == 500){
            alert("Hubo un error al registrar el usuario, consulte con el administrador");
          }
        }
      );
    }


  }


  public actualizar():void {

    this.usuarioService.actualizar(this.usuarioForm.value).subscribe(
      (response : number) => {

        alert("Usuario actualizado");
        console.log(response);
        this.showButtonRegistrar = true;
        this.showButtonActualizar = false;
        this.flagShowForm = false;
        this.listar();

      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );



  }



  public listar(): void{

    this.usuarioService.listar().subscribe(
      (response : Usuario[]) => {

        console.log(response);
        this.usuarios = response;
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );

  }


}
