import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Producto} from "../models/producto";

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private apiServicioUrl = 'http://localhost:8082';

  constructor(private http: HttpClient) {

  }

  public listar(): Observable<Producto[]>{
    return this.http.get<Producto[]>(`${this.apiServicioUrl}/api/valtx/producto`);
  }

  public registrar(producto: Producto) : Observable<number>{
    return this.http.post<number>(`${this.apiServicioUrl}/api/valtx/producto`,producto);
  }

  public actualizar(producto: Producto) : Observable<number>{
    return this.http.put<number>(`${this.apiServicioUrl}/api/valtx/producto`, producto);
  }

  public eliminar(codProducto: string) : Observable<number>{
    return this.http.delete<number>(`${this.apiServicioUrl}/api/valtx/producto/${codProducto}`);
  }


}
