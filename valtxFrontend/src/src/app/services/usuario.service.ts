import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Usuario} from "../models/usuario";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private apiServicioUrl = 'http://localhost:8082';

  constructor(private http: HttpClient) {

  }

  public listar(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(`${this.apiServicioUrl}/api/valtx/usuario`);
  }

  public registrar(usuario: Usuario) : Observable<number>{
    return this.http.post<number>(`${this.apiServicioUrl}/api/valtx/usuario`,usuario);
  }

  public actualizar(usuario: Usuario) : Observable<number>{
    return this.http.put<number>(`${this.apiServicioUrl}/api/valtx/usuario`, usuario);
  }

  public eliminar(codUsuario: string) : Observable<number>{
    return this.http.delete<number>(`${this.apiServicioUrl}/api/valtx/usuario/${codUsuario}`);
  }
}
