import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Requerimiento} from '../requerimiento';

@Injectable({
  providedIn: 'root'
})
export class RequerimientoService {

  private apiServicioUrl = 'http://localhost:8082';

  constructor(private http: HttpClient) {

  }

  public listarRequerimientos(): Observable<Requerimiento[]>{
    return this.http.get<Requerimiento[]>(`${this.apiServicioUrl}/api/acerosarequipa/requerimiento`);
  }

  public agregarRequerimiento(requerimiento: Requerimiento) : Observable<number>{
    return this.http.post<number>(`${this.apiServicioUrl}/api/acerosarequipa/requerimiento`,requerimiento);
  }

  public actualizarRequerimiento(requerimiento: Requerimiento) : Observable<number>{
    return this.http.put<number>(`${this.apiServicioUrl}/api/acerosarequipa/requerimiento`, requerimiento);
  }

  public eliminarRequerimiento(idRequerimiento: number) : Observable<number>{
    return this.http.delete<number>(`${this.apiServicioUrl}/api/acerosarequipa/requerimiento/${idRequerimiento}`);
  }

  public registrarFileAnexo(formularioData: FormData, idRequerimiento: string):any{
    // const options = {
    //   headers: new HttpHeaders({
    //     idRequerimiento : idRequerimiento,
    //     file1 : formularioData
    //   })
    // };

    return this.http.post<any>(`${this.apiServicioUrl}/api/acerosarequipa/requerimiento/imagen/${idRequerimiento}`,formularioData);
  }



}
