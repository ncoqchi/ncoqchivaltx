import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Sucursal} from "../models/sucursal";

@Injectable({
  providedIn: 'root'
})
export class SucursalService {

  private apiServicioUrl = 'http://localhost:8082';

  constructor(private http: HttpClient) {
  }

  public listar(): Observable<Sucursal[]>{
    return this.http.get<Sucursal[]>(`${this.apiServicioUrl}/api/valtx/sucursal`);
  }

  public registrar(sucursal: Sucursal) : Observable<number>{
    return this.http.post<number>(`${this.apiServicioUrl}/api/valtx/sucursal`,sucursal);
  }

  public actualizar(sucursal: Sucursal) : Observable<number>{
    return this.http.put<number>(`${this.apiServicioUrl}/api/valtx/sucursal`, sucursal);
  }

  public eliminar(codSucursal: string) : Observable<number>{
    return this.http.delete<number>(`${this.apiServicioUrl}/api/valtx/sucursal/${codSucursal}`);
  }

}
