export interface Pregunta{
  id: number;
  enunciado: string;
  imagenEnunciado : string;
  alternativa1: string;
  imagenAlternativa1:string;
  alternativa2: string;
  imagenAlternativa2:string;
  alternativa3 : string;
  imagenAlternativa3:string;
  alternativa4: string;
  imagenAlternativa4:string;
  respuesta: string;
  respuestaTexto: string
}
