import {Sucursal} from "./sucursal";

export interface Usuario{
  codUsuario : string;
  nombre : string;
  user : number;
  password :string;
  sucursal : Sucursal
}
