export interface Requerimiento{
 id: number;
 nombre: string;
 apellido: string;
 area: string;
 solicitud : string;
 descripcion: string;
 anexo: string;

}
