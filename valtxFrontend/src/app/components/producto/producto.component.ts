import { Component, OnInit } from '@angular/core';

import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Producto} from '../../models/producto';
import {ProductoService} from '../../services/producto.service';


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  productoForm: FormGroup = new FormGroup({});
  productos: Producto[] = [];
  flagShowForm : boolean=false;
  showButtonRegistrar : boolean=false;
  showButtonActualizar : boolean=false;

  constructor(private formBuilder: FormBuilder,private productoService: ProductoService) { }

  ngOnInit(): void {

    this.listar();

    this.productoForm = this.formBuilder.group({
      codProducto : [''],
      nombre : [''],
      precio : [0.0]
    });
  }


  public showForm(){
    this.flagShowForm = true;

    console.log(this.flagShowForm);
  }

  public cancelarForm(){

    this.flagShowForm = false;
    console.log(this.flagShowForm)
  }
  public nuevo() : void {

    this.flagShowForm = true;
    this.showButtonRegistrar = true;
    this.showButtonActualizar = false;

    this.productoForm.patchValue({
      codProducto : '',
      nombre: '',
      precio : 0.0
    });
  }



  public editar(producto: Producto): void{

    this.flagShowForm = true;
    this.showButtonRegistrar = false;
    this.showButtonActualizar = true;

    console.log(producto);
    this.productoForm.patchValue({
      codProducto : producto.codProducto,
      nombre: producto.nombre,
      precio : producto.precio
    });


  }

  public registrar():void {

    this.productoService.registrar(this.productoForm.value).subscribe(
      (response : number) => {
        console.log(response);
        alert("Producto Registrado");
        this.showButtonRegistrar = true;
        this.showButtonActualizar = false;
        this.flagShowForm = false;
        this.listar();
      },
      (error: HttpErrorResponse) => {
        console.error(error);
        if(error.status == 500){
          alert("Hubo un error al registrar el Producto, consulte con el administrador");
        }
      }
    );
  }


  public actualizar():void {

    this.productoService.actualizar(this.productoForm.value).subscribe(
      (response : number) => {
        console.log("actualizarProducto");
        alert("Producto actualizado");
        console.log(response);
        this.showButtonRegistrar = true;
        this.showButtonActualizar = false;
        this.flagShowForm = false;

        this.listar();
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );
  }


  public eliminar( codProducto: string) : void {
    var a = confirm("¿Desea eliminar el producto Seleccionado?");
    console.log(a);
    if(a){
      this.productoService.eliminar(codProducto).subscribe(
        (response : number) => {
          console.log(response);
          alert("Producto Eliminado");
          this.listar();
        },
        (error: HttpErrorResponse) => {
          console.error(error);
        }
      );
    }
  }

  public listar(): void{

      this.productoService.listar().subscribe(
        (response : Producto[]) => {

          console.log(response);
          this.productos = response;
        },
        (error: HttpErrorResponse) => {
          console.error(error);
        }
      );

  }

}
