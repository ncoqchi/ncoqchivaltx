import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Sucursal} from "../../models/sucursal";
import {SucursalService} from "../../services/sucursal.service";

@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.css']
})
export class SucursalComponent implements OnInit {

  sucursalForm: FormGroup = new FormGroup({});
  public sucursals : Sucursal[] = [];
  flagShowForm : boolean=false;
  showButtonRegistrar : boolean=false;
  showButtonActualizar : boolean=false;

  constructor(private formBuilder: FormBuilder,private sucursalService: SucursalService) { }

  ngOnInit(): void {

    this.listar();

    this.sucursalForm = this.formBuilder.group({
      codSucursal : [''],
      nombre : ['']
    });
  }


  public showForm(){
    this.flagShowForm = true;

    console.log(this.flagShowForm);
  }

  public cancelarForm(){

    this.flagShowForm = false;
    console.log(this.flagShowForm)
  }
  public nuevo() : void {

    this.flagShowForm = true;
    this.showButtonRegistrar = true;
    this.showButtonActualizar = false;

    this.sucursalForm.patchValue({
      codSucursal : '',
      nombre: ''
    });
  }



  public editar(sucursal: Sucursal): void{

    this.flagShowForm = true;
    this.showButtonRegistrar = false;
    this.showButtonActualizar = true;

    console.log(sucursal);
    this.sucursalForm.patchValue({
      codSucursal : sucursal.codSucursal,
      nombre: sucursal.nombre
    });


  }

  public eliminar( codSucursal: string) : void {

    var a = confirm("¿Desea eliminar la sucursal Seleccionado?");
    console.log(a);
    if(a){
      this.sucursalService.eliminar(codSucursal).subscribe(
        (response : number) => {
          console.log(response);
          alert("Sucursal Eliminada");
          this.listar();
        },
        (error: HttpErrorResponse) => {
          console.error(error);
          if(error.status == 500){
            alert("No se puede eliminar la sucursal, esta asignada a algunos usuarios");
            this.listar();
          }
        }
      );
    }

  }

  public registrar():void {

    this.sucursalService.registrar(this.sucursalForm.value).subscribe(
      (response : number) => {
        console.log(response);
        alert("Sucursal Registrada");
        this.showButtonRegistrar = true;
        this.showButtonActualizar = false;
        this.flagShowForm = false;
        this.listar();
      },
      (error: HttpErrorResponse) => {
        console.error(error);
        if(error.status == 500){
          alert("Hubo un error al registrar la sucursal, consulte con el administrador");
        }
      }
    );
  }


  public actualizar():void {

    this.sucursalService.actualizar(this.sucursalForm.value).subscribe(
      (response : number) => {
        alert("Sucursal Actualizada");
        console.log(response);
        this.showButtonRegistrar = true;
        this.showButtonActualizar = false;
        this.flagShowForm = false;

        this.listar();
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );
  }

  public listar(): void {

    this.sucursalService.listar().subscribe(
      (response : Sucursal[]) => {
        console.log(response);
        this.sucursals = response;
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );

  }

}
