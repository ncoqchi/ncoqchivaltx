import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductoComponent} from "./components/producto/producto.component";
import {SucursalComponent} from "./components/sucursal/sucursal.component";
import {UsuarioComponent} from "./components/usuario/usuario.component";
import {AppComponent} from './app.component';
import {HomeComponent} from './components/home/home.component';

const routes: Routes = [
  { path: 'producto', component: ProductoComponent },
  { path: 'sucursal', component: SucursalComponent },
  { path: 'usuario', component: UsuarioComponent},
  { path: 'home', component: HomeComponent},
  { path: '', redirectTo:'/home', pathMatch : 'full'},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
