CREATE DATABASE  IF NOT EXISTS `dbvaltx` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbvaltx`;

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `cod_producto` char(2) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  PRIMARY KEY (`cod_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES ('01','CAMISETAS  PERU 2',45.79),('02','CARTERAS',45.98),('03','LAPTOP HP 4320S PLUS',3600.00),('04','POLOS ADIDAS',121.00);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal` (
  `cod_sucursal` char(2) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`cod_sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal`
--

LOCK TABLES `sucursal` WRITE;
/*!40000 ALTER TABLE `sucursal` DISABLE KEYS */;
INSERT INTO `sucursal` VALUES ('01','AMERICA'),('02','AFRICA'),('03','PERÚS');
/*!40000 ALTER TABLE `sucursal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `cod_usuario` char(2) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `cod_sucursal` char(2) NOT NULL,
  PRIMARY KEY (`cod_usuario`),
  KEY `fk_sucursal` (`cod_sucursal`),
  CONSTRAINT `fk_sucursal` FOREIGN KEY (`cod_sucursal`) REFERENCES `sucursal` (`cod_sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('01','Nelson Coqchi Apaza','ncoqchi','1234','01'),('02','ADMIN','admonpeldap','12345678A$','02'),('03','REPORTS','admonpeldap','12345678A$ghjkiug','03');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;


DELIMITER ;;
CREATE  PROCEDURE `sp_delete_producto`(
    IN p_cod_producto VARCHAR(2)
)
BEGIN

	DELETE FROM producto where cod_producto = p_cod_producto;

END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_delete_sucursal`(
    IN p_cod_sucursal VARCHAR(2)
)
BEGIN

	DELETE FROM sucursal where cod_sucursal = p_cod_sucursal;

END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_delete_usuario`(
    IN p_cod_usuario VARCHAR(2)
)
BEGIN

	DELETE FROM usuario where cod_usuario = p_cod_usuario;

END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_get_productos`()
BEGIN
   SELECT cod_producto as codProducto, nombre, precio from producto;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_get_sucursal`()
BEGIN
   SELECT cod_sucursal as codSucursal, nombre from sucursal;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_get_usuarios`()
BEGIN
   select u.cod_usuario, u.nombre, u.user, u.password, s.cod_sucursal,  s.nombre as nombre_sucursal 
   from usuario u inner join sucursal s on u.cod_sucursal = s.cod_sucursal;
END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_insertar_producto`(
    IN p_cod_producto VARCHAR(2),
    IN p_nombre VARCHAR(50) ,
    IN p_precio DECIMAL(10,2)
)
BEGIN
    INSERT INTO producto VALUES(p_cod_producto, p_nombre, p_precio);
END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_insertar_sucursal`(
    IN p_cod_sucursal VARCHAR(2),
    IN p_nombre VARCHAR(50)
)
BEGIN
    INSERT INTO sucursal VALUES(p_cod_sucursal, p_nombre);
END ;;
DELIMITER ;


DELIMITER ;;
CREATE  PROCEDURE `sp_insertar_usuario`(
    IN p_cod_usuario VARCHAR(2),
    IN p_nombre VARCHAR(50) ,
    IN p_user VARCHAR(50) ,
    IN p_password VARCHAR(50) ,
    IN p_cod_sucursal VARCHAR(2)
)
BEGIN
    INSERT INTO usuario VALUES(p_cod_usuario, p_nombre, p_user, p_password, p_cod_sucursal);
END ;;
DELIMITER ;

DELIMITER ;;
CREATE  PROCEDURE `sp_update_producto`(
    IN p_cod_producto VARCHAR(2),
    IN p_nombre VARCHAR(50) ,
    IN p_precio DECIMAL(10,2)
)
BEGIN
    UPDATE producto SET 
    nombre = p_nombre,
    precio = p_precio WHERE cod_producto = p_cod_producto;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE  PROCEDURE `sp_update_sucursal`(
    IN p_cod_sucursal VARCHAR(2),
    IN p_nombre VARCHAR(50) 
)
BEGIN

    UPDATE sucursal SET 
    nombre = p_nombre WHERE cod_sucursal = p_cod_sucursal;

END ;;
DELIMITER ;

DELIMITER ;;
CREATE  PROCEDURE `sp_update_usuario`(
    IN p_cod_usuario VARCHAR(2),
    IN p_nombre VARCHAR(50) ,
    IN p_user VARCHAR(50) ,
    IN p_password VARCHAR(50) ,
    IN p_cod_sucursal VARCHAR(2)
)
BEGIN
    UPDATE usuario SET
    nombre = p_nombre, 
    user = p_user, 
    password = p_password, 
    cod_sucursal = p_cod_sucursal where cod_usuario = p_cod_usuario;
    
END ;;
DELIMITER ;
