CREATE TABLE `requerimiento` (
                                 `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                 `nombre` varchar(255) DEFAULT NULL,
                                 `apellido` varchar(255) DEFAULT NULL,
                                 `area` varchar(255) DEFAULT NULL,
                                 `solicitud` varchar(255) DEFAULT NULL,
                                 `descripcion` varchar(255) DEFAULT NULL,
                                 `anexo` varchar(255) DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
