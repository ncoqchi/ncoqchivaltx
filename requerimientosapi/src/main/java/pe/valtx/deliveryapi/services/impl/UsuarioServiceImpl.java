package pe.valtx.deliveryapi.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pe.valtx.deliveryapi.dao.UsuarioDao;
import pe.valtx.deliveryapi.model.Usuario;
import pe.valtx.deliveryapi.services.UsuarioService;

import java.util.List;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Qualifier("usuarioDaoJBDC")
    @Autowired
    UsuarioDao usuarioDao;

    @Override
    public List<Usuario> listar() {
        return usuarioDao.listar();
    }

    @Override
    public int registrar(Usuario usuario) {


        try{
            usuarioDao.registrar(usuario);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public int actualizar(Usuario usuario) {
        try{
            usuarioDao.actualizar(usuario);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public int eliminar(String codUsuario) {

        try{
            usuarioDao.eliminar(codUsuario);
            return 1;
        }catch (Exception e){
            return 0;
        }

    }
}
