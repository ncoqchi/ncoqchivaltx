package pe.valtx.deliveryapi.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pe.valtx.deliveryapi.dao.ProductoDao;
import pe.valtx.deliveryapi.model.Producto;
import pe.valtx.deliveryapi.services.ProductoService;

import java.util.List;

@Service
public class ProductoServiceImpl implements ProductoService {

    @Qualifier("productoDaoJBDC")
    @Autowired
    ProductoDao productoDao;

    @Override
    public List<Producto> listar() {
        return productoDao.listar();
    }

    @Override
    public int registrar(Producto producto) {
        try{
            productoDao.registrar(producto);
            return 1;
        }catch (Exception e){
            return 0;
        }

    }

    @Override
    public int actualizar(Producto producto) {

        try{
            productoDao.actualizar(producto);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public int eliminar(String codProducto) {


        try{
            productoDao.eliminar(codProducto);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }
}
