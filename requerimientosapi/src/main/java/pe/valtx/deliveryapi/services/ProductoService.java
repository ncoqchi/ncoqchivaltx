package pe.valtx.deliveryapi.services;

import pe.valtx.deliveryapi.model.Producto;

import java.util.List;

public interface ProductoService {

    public List<Producto> listar();

    public int registrar(Producto producto);

    public int actualizar(Producto producto);

    public int eliminar(String codProducto);

}
