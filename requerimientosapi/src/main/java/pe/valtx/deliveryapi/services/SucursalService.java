package pe.valtx.deliveryapi.services;

import pe.valtx.deliveryapi.model.Sucursal;

import java.util.List;

public interface SucursalService {

    public List<Sucursal> listar();

    public int registrar(Sucursal producto);

    public int actualizar(Sucursal producto);

    public int eliminar(String codSucursal);
}
