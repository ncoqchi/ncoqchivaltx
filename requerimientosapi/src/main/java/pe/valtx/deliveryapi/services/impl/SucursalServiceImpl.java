package pe.valtx.deliveryapi.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pe.valtx.deliveryapi.dao.SucursalDao;
import pe.valtx.deliveryapi.model.Sucursal;
import pe.valtx.deliveryapi.services.SucursalService;

import java.util.List;

@Service
public class SucursalServiceImpl implements SucursalService {

    @Qualifier("sucursalDaoJBDC")
    @Autowired
    SucursalDao sucursalDao;

    @Override
    public List<Sucursal> listar() {
        return sucursalDao.listar();
    }

    @Override
    public int registrar(Sucursal sucursal) {
        try{
            sucursalDao.registrar(sucursal);
            return 1;
        }catch (Exception e){
            return 0;
        }

    }

    @Override
    public int actualizar(Sucursal sucursal) {

        try{
            sucursalDao.actualizar(sucursal);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public int eliminar(String codSucursal) {
        try{
            sucursalDao.eliminar(codSucursal);
            return 1;
        }catch (Exception e){
            return 0;
        }
    }
}
