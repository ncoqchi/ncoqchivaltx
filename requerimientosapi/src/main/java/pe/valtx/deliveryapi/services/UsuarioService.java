package pe.valtx.deliveryapi.services;

import pe.valtx.deliveryapi.model.Usuario;

import java.util.List;

public interface UsuarioService {

    public List<Usuario> listar();

    public int registrar(Usuario producto);

    public int actualizar(Usuario producto);

    public int eliminar(String codUsuario);
}
