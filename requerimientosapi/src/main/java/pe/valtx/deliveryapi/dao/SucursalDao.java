package pe.valtx.deliveryapi.dao;

import pe.valtx.deliveryapi.model.Sucursal;

import java.util.List;


public interface SucursalDao {

    public List<Sucursal> listar();

    public void registrar(Sucursal sucursal);

    public void actualizar(Sucursal sucursal);

    public void eliminar(String codSucursal);

}
