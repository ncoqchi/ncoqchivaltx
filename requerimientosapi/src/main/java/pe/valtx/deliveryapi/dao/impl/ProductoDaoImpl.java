package pe.valtx.deliveryapi.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.valtx.deliveryapi.dao.ProductoDao;
import pe.valtx.deliveryapi.model.Producto;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Repository("productoDaoJBDC")
public class ProductoDaoImpl implements ProductoDao {


    private static final Logger LOGGER = LogManager.getLogger(ProductoDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall simpleJdbcCall;
    private SimpleJdbcCall jdbcCallInsert;
    private SimpleJdbcCall jdbcCallUpdate;
    private SimpleJdbcCall jdbcCallDelete;




    @PostConstruct
    void init() {
        // o_name and O_NAME, same
        jdbcTemplate.setResultsMapCaseInsensitive(true);

//        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
//                .withProcedureName("getProductos").returningResultSet("productos", new RowMapper<Producto>() {
//                    @Override
//                    public Producto mapRow(ResultSet rs, int rowNum) throws SQLException {
//                        Producto producto = new Producto();
//                        producto.setCodProducto(rs.getString("cod_producto"));
//                        producto.setNombre(rs.getString("nombre"));
//                        producto.setPrecio(rs.getDouble("precio"));
//                        return producto;
//                    }
//                });
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_get_productos").returningResultSet("productos", BeanPropertyRowMapper.newInstance(Producto.class));


        jdbcCallInsert = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_insertar_producto");

        jdbcCallUpdate = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_update_producto");

        jdbcCallDelete = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_delete_producto");

    }


    @Override
    public List<Producto> listar() {

        //SqlParameterSource in = new MapSqlParameterSource()
         //       .addValue("p_id", "valor");


        Map<String, Object> out = simpleJdbcCall.execute();

        List<Producto> listaProductos = (List<Producto>) out.get("productos");
        LOGGER.info("Listado de Productos "+listaProductos);

        return listaProductos;


    }

    @Override
    public void registrar(Producto producto) {

        LOGGER.info("Producto a registrar " + producto);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("p_cod_producto", producto.getCodProducto())
                .addValue("p_nombre", producto.getNombre())
                .addValue("p_precio", producto.getPrecio());

        Map<String, Object> respuesta = jdbcCallInsert.execute(parameters);
        LOGGER.info("RES - registrarUsuario "+respuesta);

    }

    @Override
    public void actualizar(Producto producto) {
        LOGGER.info("Producto a actualizar " + producto);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("p_cod_producto", producto.getCodProducto())
                .addValue("p_nombre", producto.getNombre())
                .addValue("p_precio", producto.getPrecio());

        Map<String, Object> respuesta = jdbcCallUpdate.execute(parameters);
        LOGGER.info("RES - actualizar Usuario "+respuesta);
    }

    @Override
    public void eliminar(String codProducto) {

        LOGGER.info("Producto a eliminar " + codProducto);

        SqlParameterSource parameters = new MapSqlParameterSource().addValue("p_cod_producto", codProducto);

        Map<String, Object> respuesta = jdbcCallDelete.execute(parameters);
        LOGGER.info("RES - eliminar Usuario "+respuesta);

    }
}
