package pe.valtx.deliveryapi.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import pe.valtx.deliveryapi.model.Producto;

import javax.annotation.PostConstruct;
import java.util.List;


public interface ProductoDao {

    public List<Producto> listar();

    public void registrar(Producto producto);

    public void actualizar(Producto producto);

    public void eliminar(String codProducto);

}
