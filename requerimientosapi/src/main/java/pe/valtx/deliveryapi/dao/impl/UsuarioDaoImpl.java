package pe.valtx.deliveryapi.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.valtx.deliveryapi.dao.UsuarioDao;
import pe.valtx.deliveryapi.model.Producto;
import pe.valtx.deliveryapi.model.Usuario;
import pe.valtx.deliveryapi.model.Sucursal;
import pe.valtx.deliveryapi.model.Usuario;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository("usuarioDaoJBDC")
public class UsuarioDaoImpl implements UsuarioDao {

    private static final Logger LOGGER = LogManager.getLogger(UsuarioDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private SimpleJdbcCall simpleJdbcCall;

    private SimpleJdbcCall jdbcCallInsert;
    private SimpleJdbcCall jdbcCallUpdate;
    private SimpleJdbcCall jdbcCallDelete;


    @PostConstruct
    void init() {
        // o_name and O_NAME, same
        jdbcTemplate.setResultsMapCaseInsensitive(true);

        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_get_usuarios").returningResultSet("usuarios", new RowMapper<Usuario>() {
                    @Override
                    public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Usuario u = new Usuario();
                        u.setCodUsuario(rs.getString("cod_usuario"));
                        u.setNombre(rs.getString("nombre"));
                        u.setUser(rs.getString("user"));
                        u.setPassword(rs.getString("password"));

                        Sucursal sucursal = new Sucursal();
                        sucursal.setCodSucursal(rs.getString("cod_sucursal"));
                        sucursal.setNombre(rs.getString("nombre_sucursal"));

                        u.setSucursal(sucursal);
                        return u;
                    }
                });

        jdbcCallInsert = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_insertar_usuario");

        jdbcCallUpdate = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_update_usuario");

        jdbcCallDelete = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_delete_usuario");

    }


    @Override
    public List<Usuario> listar() {

        Map<String, Object> out = simpleJdbcCall.execute();
        List<Usuario> lista = (List<Usuario>) out.get("usuarios");

        LOGGER.info("Listado de Usuarios "+lista);
        return lista;


    }


    @Override
    public void registrar(Usuario usuario) {

        LOGGER.info("Usuario a registrar " + usuario);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("p_cod_usuario", usuario.getCodUsuario())
                .addValue("p_nombre", usuario.getNombre())
                .addValue("p_user", usuario.getUser())
                .addValue("p_password", usuario.getPassword())
                .addValue("p_cod_sucursal", usuario.getSucursal().getCodSucursal());

        Map<String, Object> respuesta = jdbcCallInsert.execute(parameters);
        LOGGER.info("RES - registrarUsuario "+respuesta);

    }

    @Override
    public void actualizar(Usuario usuario) {
        LOGGER.info("Usuario a actualizar " + usuario);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("p_cod_usuario", usuario.getCodUsuario())
                .addValue("p_nombre", usuario.getNombre())
                .addValue("p_user", usuario.getUser())
                .addValue("p_password", usuario.getPassword())
                .addValue("p_cod_sucursal", usuario.getSucursal().getCodSucursal());

        Map<String, Object> respuesta = jdbcCallUpdate.execute(parameters);
        LOGGER.info("RES - actualizar Usuario "+respuesta);
    }

    @Override
    public void eliminar(String codUsuario) {

        LOGGER.info("Usuario a eliminar " + codUsuario);

        SqlParameterSource parameters = new MapSqlParameterSource().addValue("p_cod_usuario", codUsuario);

        Map<String, Object> respuesta = jdbcCallDelete.execute(parameters);
        LOGGER.info("RES - eliminar Usuario "+respuesta);

    }
}
