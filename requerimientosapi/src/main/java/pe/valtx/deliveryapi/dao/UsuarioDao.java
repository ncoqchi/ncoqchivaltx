package pe.valtx.deliveryapi.dao;


import pe.valtx.deliveryapi.model.Usuario;

import java.util.List;


public interface UsuarioDao {

    public List<Usuario> listar();

    public void registrar(Usuario usuario);

    public void actualizar(Usuario usuario);

    public void eliminar(String codUsuario);

}
