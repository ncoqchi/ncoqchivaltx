package pe.valtx.deliveryapi.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.valtx.deliveryapi.dao.SucursalDao;
import pe.valtx.deliveryapi.model.Sucursal;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Repository("sucursalDaoJBDC")
public class SucursalDaoImpl implements SucursalDao {

    private static final Logger LOGGER = LogManager.getLogger(SucursalDaoImpl.class);
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall simpleJdbcCall;
    private SimpleJdbcCall jdbcCallInsert;
    private SimpleJdbcCall jdbcCallUpdate;
    private SimpleJdbcCall jdbcCallDelete;


    @PostConstruct
    void init() {
        // o_name and O_NAME, same
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_get_sucursal").returningResultSet("sucursal", BeanPropertyRowMapper.newInstance(Sucursal.class));


        jdbcCallInsert = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_insertar_sucursal");

        jdbcCallUpdate = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_update_sucursal");

        jdbcCallDelete = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("sp_delete_sucursal");

    }


    @Override
    public List<Sucursal> listar() {

        //SqlParameterSource in = new MapSqlParameterSource()
         //       .addValue("p_id", "valor");

        Map<String, Object> out = simpleJdbcCall.execute();

        List<Sucursal> listaSucursals = (List<Sucursal>) out.get("sucursal");
        LOGGER.info("Listado de Sucursales: "+listaSucursals);

        return listaSucursals;

    }

    @Override
    public void registrar(Sucursal sucursal) {

        LOGGER.info("Sucursal a registrar " + sucursal);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("p_cod_sucursal", sucursal.getCodSucursal())
                .addValue("p_nombre", sucursal.getNombre());

        Map<String, Object> respuesta = jdbcCallInsert.execute(parameters);

        LOGGER.info("RES - registrarUsuario "+respuesta);

    }

    @Override
    public void actualizar(Sucursal sucursal) {
        LOGGER.info("Sucursal a actualizar " + sucursal);

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("p_cod_sucursal", sucursal.getCodSucursal())
                .addValue("p_nombre", sucursal.getNombre());

        Map<String, Object> respuesta = jdbcCallUpdate.execute(parameters);

        LOGGER.info("RES - actualizar Usuario "+respuesta);
    }

    @Override
    public void eliminar(String codSucursal) {

        LOGGER.info("Sucursal a eliminar " + codSucursal);

        SqlParameterSource parameters = new MapSqlParameterSource().addValue("p_cod_sucursal", codSucursal);

        Map<String, Object> respuesta = jdbcCallDelete.execute(parameters);

        LOGGER.info("RES - eliminar Usuario "+respuesta);

    }
}
