package pe.valtx.deliveryapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import pe.valtx.deliveryapi.model.Usuario;
import pe.valtx.deliveryapi.dao.UsuarioDao;
import pe.valtx.deliveryapi.services.UsuarioService;

import javax.annotation.PostConstruct;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/valtx")
public class UsuarioController {


    @Autowired
    private UsuarioService usuarioService;

    @PostConstruct
    public void init() {
        
    }

    @GetMapping(path = "/usuario")
    public List<Usuario> listarUsuario() {
        List<Usuario> usuarios =usuarioService.listar();
        System.out.println(usuarios);
        return usuarios;
    }

    @DeleteMapping(path = "/usuario/{cod_usuario}")
    public int eliminarUsuario(@PathVariable("cod_usuario") String codUsuario) {
        usuarioService.eliminar(codUsuario);

        return 1;
    }

    @PostMapping(path = "/usuario")
    public int registrarUsuario(@RequestBody Usuario usuario) {
        usuarioService.registrar(usuario);
        return 1;
    }

    @PutMapping(path = "/usuario")
    public int actualizarUsuario(@RequestBody Usuario usuario) {
        usuarioService.actualizar(usuario);
        return 1;
    }

}
