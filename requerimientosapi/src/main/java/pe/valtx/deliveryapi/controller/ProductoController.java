package pe.valtx.deliveryapi.controller;

import pe.valtx.deliveryapi.dao.ProductoDao;
import pe.valtx.deliveryapi.model.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.valtx.deliveryapi.services.ProductoService;

import javax.annotation.PostConstruct;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/valtx")
public class ProductoController {


    @Autowired
    private ProductoService productoService;

    @PostConstruct
    public void init() {
        
    }

    @GetMapping(path = "/producto")
    public List<Producto> listarProductos()
    {
//        return productoDao.listar();
        return productoService.listar();
    }

    @DeleteMapping(path = "/producto/{codProducto}")
    public int eliminarProducto(@PathVariable("codProducto") String codProducto) {
        return productoService.eliminar(codProducto);
    }
//
    @PostMapping(path = "/producto")
    public int registrarProducto(@RequestBody Producto producto) {

        return productoService.registrar(producto);

//        return 1;
    }
//
    @PutMapping(path = "/producto")
    public int actualizarProducto(@RequestBody Producto producto) {
        return productoService.actualizar(producto);
//        return 1;
    }

}
