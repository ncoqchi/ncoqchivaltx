package pe.valtx.deliveryapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import pe.valtx.deliveryapi.dao.SucursalDao;
import pe.valtx.deliveryapi.model.Sucursal;
import pe.valtx.deliveryapi.services.SucursalService;

import javax.annotation.PostConstruct;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/valtx")
public class SucursalController {

    
    @Autowired
    private SucursalService sucursalService;

    @PostConstruct
    public void init() {
        
    }

    @GetMapping(path = "/sucursal")
    public List<Sucursal> listarSucursales() {
        return sucursalService.listar();
    }

    @DeleteMapping(path = "/sucursal/{cod_sucursal}")
    public int eliminarSucursal(@PathVariable("cod_sucursal") String codSucursal) {
        return sucursalService.eliminar(codSucursal);
//        return 1;
    }

    @PostMapping(path = "/sucursal")
    public int registrarSucursal(@RequestBody Sucursal sucursal) {
        return sucursalService.registrar(sucursal);
//        return 1;
    }

    @PutMapping(path = "/sucursal")
    public int actualizarSucursal(@RequestBody Sucursal sucursal) {
        return sucursalService.actualizar(sucursal);
//        return 1;
    }

}
