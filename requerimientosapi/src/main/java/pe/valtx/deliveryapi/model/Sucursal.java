package pe.valtx.deliveryapi.model;

import java.io.Serializable;

public class Sucursal implements Serializable {

    private String codSucursal;

    private String nombre;

    public Sucursal() {
    }

    public Sucursal(String codSucursal, String nombre) {

        this.codSucursal = codSucursal;
        this.nombre = nombre;

    }

    public String getCodSucursal() {
        return codSucursal;
    }

    public void setCodSucursal(String codSucursal) {
        this.codSucursal = codSucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public String toString() {
        return "Sucursal{" +
                "codSucursal='" + codSucursal + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
