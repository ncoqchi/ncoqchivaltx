package pe.valtx.deliveryapi.model;

import java.io.Serializable;

public class Usuario implements Serializable {

    private String codUsuario;
    private String nombre;
    private String user;
    private String password;
    private Sucursal sucursal;

    public Usuario() {
    }

    public Usuario(String codUsuario, String nombre, String user, String password) {
        this.codUsuario = codUsuario;
        this.nombre = nombre;
        this.user = user;
        this.password = password;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "codUsuario='" + codUsuario + '\'' +
                ", nombre='" + nombre + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", sucursal=" + sucursal +
                '}';
    }
}
