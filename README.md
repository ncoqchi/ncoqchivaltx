# ncoqchivaltx

Desarrollo del caso practico de VALTX

# autor

Nelson Coqchi Apaza


## Installation
1. Ingresar al Proyecto Frontend "valtxFrontend" y ejecutar el siguiente comando
```bash
npm install
```

2. Terminado ejecutar
```bash
ng serve
```

3. Finalmente acceder a
```bash
http://localhost:4200
```


4. Ingresar al Proyecto Backend "requerimientosapi" y descargar las dependencias del proyecto pom

5. Ingresar al Proyecto Backend "requerimientosapi" en la ruta src/main/resources, se encuentra la ruta del script
```bash
createDb.sql
```


6. El proyecto backend se ha desplegado en el puerto 8082, modificar el application.properties de ser necesario 
```bash
spring.datasource.url = jdbc:mysql://localhost:3306/dbvaltx?useSSL=false
spring.datasource.username = usrTodos
spring.datasource.password = 1234
server.port=8082
```

7. Ejecutar el proyecto Pom, la clase Principal 
```bash
pe.valtx.deliveryapi.Application
```



